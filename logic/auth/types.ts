/**
 * This are inner types, used only inside the client
 */
import {
  RawTokenType,
  RawCredentialsType,
  RawRegistrationDataType
} from './models'

export type Token = RawTokenType
export type RegistrationData = RawRegistrationDataType
export type Credentials = RawCredentialsType
