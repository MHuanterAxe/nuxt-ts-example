import * as tPromise from 'io-ts-promise'

import { Service } from 'vue-typedi'
import { Either, right, left } from 'fp-ts/Either'
import tokens from '~/logic/tokens'
import ApiService from '~/logic/services/api-service'
import { RawToken, RawTokenType } from '~/logic/auth/models'
import { Credentials, RegistrationData } from '~/logic/auth/types'

@Service(tokens.AUTH_API_SERVICE)
/**
 * Service class to authenticate and register user
 *
 * Is injected into the module context to be used
 * Should not be used as-is, only as a part of the module!
 */
export default class AuthApiService extends ApiService {
  /**
   * Method that sends registration data
   *
   * @returns Parsed response data.
   */
  public async register (data: RegistrationData): Promise<Either<Error, RawTokenType>> {
    try {
      const response = await this.$axios.post('auth/register', data)

      this.setTokenToDefaults(response.data.access_token)

      const result = await tPromise.decode(RawToken, response.data.access_token)

      return right(result)
    } catch (error) {
      return left(Error('Регистрация не удалась!'))
    }
  }

  /**
   * Method that sends credentials to authenticate user
   *
   * @returns Parsed response data.
   */
  public async authenticate (data: Credentials): Promise<Either<Error, RawTokenType>> {
    try {
      const response = await this.$axios.post('auth/login', data)

      this.setTokenToDefaults(response.data.access_token)

      const result = await tPromise.decode(RawToken, response.data.access_token)

      return right(result)
    } catch (error) {
      return left(Error('Авторизация не удалась!'))
    }
  }

  /**
   * Method that sends token to check authentication
   *
   * @returns Parsed response data.
   */
  public async check (): Promise<Either<Error, RawTokenType>> {
    try {
      const response = await this.$axios.get('auth/check')

      this.setTokenToDefaults(response.data.access_token)

      const result = await tPromise.decode(RawToken, response.data.access_token)

      return right(result)
    } catch (error) {
      return left(Error('Не авторизован!'))
    }
  }

  /**
   * Method that do logout from application
   *
   * @returns Promise<Either<Error, boolean>>.
   */
  public async logout (): Promise<Either<Error, boolean>> {
    try {
      await this.$axios.get('auth/logout')

      this.removeTokenFromDefaults()

      return right(true)
    } catch (error) {
      return left(Error('Не удалось выполнить выход!'))
    }
  }

  /**
   * Sets default token to $axios.defaults.headers.Authorization
   *
   * @param token
   */
  public setTokenToDefaults (token: string): void {
    this.$axios.defaults.headers.Authorization = `Bearer ${token}`
  }

  public removeTokenFromDefaults(): void {
    this.$axios.defaults.headers.Authorization = ''
  }
}
