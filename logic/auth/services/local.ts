import { Service } from 'vue-typedi'
import tokens from '~/logic/tokens'
import LocalService from '~/logic/services/local'

@Service(tokens.AUTH_LOCAL_SERVICE)
/**
 * Service class to store authenticated user data
 *
 * Is injected into the module context to be used
 * Should not be used as-is, only as a part of the module!
 */
export default class AuthLocalService extends LocalService {
  protected _key: string = 'auth'
}
