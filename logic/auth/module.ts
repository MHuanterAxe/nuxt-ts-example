import { Either, fold, isLeft, isRight, left, right } from 'fp-ts/Either'
import { pipe } from 'fp-ts/function'

import { Action, Mutation, State } from 'vuex-simple'
import { Credentials, RegistrationData, Token } from '@/logic/auth/types'
import { Inject, Injectable } from 'vue-typedi'
import tokens from '../tokens'
import { AuthApiService, AuthLocalService } from './services/'
import { RawTokenType } from '~/logic/auth/models'

@Injectable()
/**
 * Auth Vuex module
 */
export class AuthModule {
  @Inject(tokens.AUTH_API_SERVICE)
  /**
   * AuthApiService represents api service of auth module
   */
  public apiService!: AuthApiService

  @Inject(tokens.AUTH_LOCAL_SERVICE)
  /**
   * AuthLocalService represents local storage service of auth module
   */
  public localService!: AuthLocalService

  @State()
  public token: Token

  constructor () {
    this.token = ''
  }

  @Mutation()
  public setToken (token: RawTokenType) {
    this.token = token
  }

  @Mutation()
  public clearToken () {
    this.token = ''
  }

  @Action()
  public async register (data: RegistrationData): Promise<Either<Error, RawTokenType>> {
    const result = await this.apiService.register(data)

    return pipe(
      result,
      fold(
        (error: Error) => {
          return left(error)
        },
        (token: string) => {
          this.setToken(token)

          this.localService.setLocalData(token)

          return right(token)
        }
      )
    )
  }

  @Action()
  public async authenticate (data: Credentials): Promise<Either<Error, RawTokenType>> {
    const result = await this.apiService.authenticate(data)

    return pipe(
      result,
      fold(
        (error: Error) => {
          return left(error)
        },
        (token: string) => {
          this.setToken(token)

          this.localService.setLocalData(token)

          return right(token)
        }
      )
    )
  }

  @Action()
  public async check (): Promise<boolean> {
    const tokenOrError = this.localService.getLocalData()

    if (isLeft(tokenOrError)) {
      return false
    }

    const result = await this.apiService.check()

    return pipe(
      result,
      fold(() => {
        return false
      }, (token: string) => {
        this.apiService.setTokenToDefaults(token)

        this.localService.setLocalData(token)

        this.setToken(token)

        return true
      })
    )
  }

  @Action()
  public async logout (): Promise<boolean> {
    const tokenOrError = this.localService.getLocalData()

    if (isLeft(tokenOrError)) {
      return false
    }

    const result = await this.apiService.logout()

    return pipe(
      result,
      fold(() => {
        return false
      }, () => {
        this.localService.removeLocalData()

        this.apiService.removeTokenFromDefaults()

        this.clearToken()

        return true
      })
    )
  }
}
