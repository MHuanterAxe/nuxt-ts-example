import * as ts from 'io-ts'

/**
 * Runtime type, that used for schema validation
 */
export const RawToken = ts.string
export const RawRegistrationData = ts.type({
  firstName: ts.string,
  lastName: ts.string,
  middleName: ts.string,
  email: ts.string,
  password: ts.string
})

export const RawCredentials = ts.type({
  email: ts.string,
  password: ts.string
})

export type RawTokenType = ts.TypeOf<typeof RawToken>
export type RawRegistrationDataType = ts.TypeOf<typeof RawRegistrationData>
export type RawCredentialsType = ts.TypeOf<typeof RawCredentials>
