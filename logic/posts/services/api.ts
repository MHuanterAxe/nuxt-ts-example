import * as ts from 'io-ts'
import * as tPromise from 'io-ts-promise'

import { Service } from 'vue-typedi'
import { RawPostType, RawPost } from '@/logic/posts/models'
import tokens from '@/logic/tokens'
import ApiService from '~/logic/services/api-service'

@Service(tokens.POSTS_SERVICE)
/**
 * Service class to fetch posts from api
 *
 * Is injected into the module contextd to be used
 * Should not be used as-is, only as a part of the module!
 */
export default class PostsService extends ApiService {
  /**
   * @returns Parsed response data.
   */
  public async fetchPosts (): Promise<RawPostType[]> {
    const response = await this.$axios.get('posts')
    return tPromise.decode(ts.array(RawPost), response.data)
  }
}
