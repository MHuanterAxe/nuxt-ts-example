/**
 * This are inner types, used only inside the client
 */
import { RawPostType } from './models'

export type PostType = RawPostType

export interface PostsPayloadType {
  title: string,
  body: string
}
