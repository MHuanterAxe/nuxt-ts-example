import * as ts from 'io-ts'

/**
 * Runtime type, thay used for schema validation
 */
export const RawPost = ts.type({
  id: ts.number,
  userId: ts.number,
  title: ts.string,
  body: ts.string
})

export type RawPostType = ts.TypeOf<typeof RawPost>
