import { State, Getter, Mutation, Action } from 'vuex-simple'
import { RawPostType } from '@/logic/posts/models'
import { Inject, Injectable } from 'vue-typedi'
import tokens from '../tokens'
import PostsService from './services/api'
import { PostType } from './types'

@Injectable()
/**
 * Posts Vuex module
 */
export class PostsModule {
  @Inject(tokens.POSTS_SERVICE)
  public service!: PostsService

  @State()
  public posts: RawPostType[]

  constructor (posts: RawPostType[] = []) {
    this.posts = posts
  }

  @Getter()
  /**
   * @returns 'true' when there are posts in state, otherwise 'false'
   */
  public get hasPosts (): boolean {
    return Boolean(this.posts && this.posts.length)
  }

  @Mutation()
  public setPosts (payload: RawPostType[]) {
    const updatePosts: PostType[] = []

    for (const post of payload.slice(0, 10)) {
      updatePosts.push({ ...post })
    }

    this.posts = updatePosts
  }

  @Action()
  /**
   * @returns Promise<RawPostType[]>
   */
  public async fetchPosts (): Promise<RawPostType[]> {
    const postList = await this.service.fetchPosts()

    this.setPosts(postList)

    return postList
  }
}
