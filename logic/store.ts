import { Module } from 'vuex-simple'

import { PostsModule } from '@/logic/posts/module'

export default class TypedStore {
  @Module()
  public posts = new PostsModule()
}
