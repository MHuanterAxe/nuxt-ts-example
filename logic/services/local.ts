import { Either, left, right } from 'fp-ts/Either'

export interface ILocalService {
  key: string
  setLocalData(data: any): void
  getLocalData(): Either<Error, string>
  removeLocalData(): void
}

export default class LocalService implements ILocalService {
  protected _key: string = ''

  public set key (key: string) {
    this._key = key
  }

  public get key (): string {
    return this._key
  }

  public setLocalData (data: any): void {
    localStorage.setItem(this._key, data)
  }

  public getLocalData (): Either<Error, string> {
    const value = localStorage.getItem(this._key)

    if (value === null || value === undefined) {
      return left(Error(`There are no data in localStorage where key = ${this._key}`))
    }

    return right(value)
  }

  public removeLocalData (): void {
    localStorage.removeItem(this._key)
  }
}
