import { Token } from 'vue-typedi'

export default {
  // Axios instance
  AXIOS: new Token('axios'),

  // Auth services
  AUTH_LOCAL_SERVICE: new Token('auth-api-service'),
  AUTH_API_SERVICE: new Token('auth-local-service'),

  // Posts service
  POSTS_SERVICE: new Token('posts-service')
}
