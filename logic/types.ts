import { PostType } from './posts/types'

export interface StateType {
  posts: {
    posts: PostType[]
  }
}
