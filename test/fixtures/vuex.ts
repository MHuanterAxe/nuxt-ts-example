import axios from 'axios'
import { vuexPlugin } from 'jest-matcher-vue-test-utils'
import faker from 'faker'
import rosie from 'rosie'
import Vuex, { Store } from 'vuex'

import { StateType } from '~/logic/types'
import { install as installDI } from '~/plugins/vue-typedi'
import createStore from '~/store'

import { PostType } from '~/logic/posts/types'
import { fakerFactory, FakerFactoryType } from '~/test/fixtures/faker'

export const postsFactory = new rosie.Factory()
  .extend<FakerFactoryType & PostType, FakerFactoryType>(fakerFactory)
  .sequence('id')
  .attr('userId', faker.random.number)
  .attr('title', faker.name.title)
  .attr('body', faker.lorem.sentences)

export const storeFactory = new rosie.Factory<Store<StateType>>()
  .option('localVue')
  .option('state', undefined)
  .after((_, options) => {
    installDI(options.localVue, axios)

    options.localVue.use(Vuex)
    const store = createStore({
      // We need this plugin to allow jest-matcher-vue-test-utils
      // assertions for Vuex:
      plugins: [vuexPlugin()],
    })

    if (options.state) {
      store.replaceState(options.state)
    }

    return store
  })
