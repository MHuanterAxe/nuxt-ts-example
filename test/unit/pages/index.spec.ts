import { createLocalVue, mount } from '@vue/test-utils'

import IndexPage from '~/pages/index.vue'
import { Store } from 'vuex'
import { StateType } from '~/logic/types'
import { storeFactory } from '~/test/fixtures/vuex'

const localVue = createLocalVue()

describe('IndexPage', () => {
  let store: Store<StateType>

  beforeEach(() => {
    store = storeFactory.build(undefined, { localVue })
  })

  describe('unit tests', () => {
    it('should be rendered successfully', () => {
      const wrapper = mount(IndexPage, {
        localVue,
        store
      })

      expect(Boolean(wrapper.vm)).toBe(true)
    })
  })

  describe('snapshot tests', () => {
    test('should match the snapshot', () => {
      const wrapper = mount(IndexPage, {
        localVue,
        store
      })

      expect(wrapper).toMatchSnapshot()
    })
  })
})
