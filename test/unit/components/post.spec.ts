import { createLocalVue, mount } from '@vue/test-utils'
import { Store } from 'vuex'

import Post from '~/components/Post.vue'

import { StateType } from '~/logic/types'
import { postsFactory, storeFactory } from '~/test/fixtures/vuex'
import { PostType } from '~/logic/posts/types'

const localVue = createLocalVue()

describe('Post component', () => {
  let post: PostType
  let store: Store<StateType>

  describe('unit tests', () => {
    beforeEach(() => {
      post = postsFactory.build()
      store = storeFactory.build(undefined, {
        localVue,
        state: {
          posts: {
            posts: [
              post
            ]
          }
        }
      })
    })

    it('should have a valid props', () => {
      expect.hasAssertions()

      expect(Post).toBeValidProps({ post }, {
        localVue,
        store
      })
    })

    it('should have correct values', () => {
      expect.hasAssertions()

      const wrapper = mount(Post, {
        store,
        localVue,
        propsData: { post }
      })

      expect(wrapper.find('.title').text().trim()).toStrictEqual(post.title)
      expect(wrapper.find('.body').text().trim()).toStrictEqual(post.body)
    })
  })

  describe('snapshot tests', () => {
    let post: PostType
    let store: Store<StateType>

    beforeAll(() => {
      // We need a seed here to be consistent for snapshot testing:
      post = postsFactory.build({}, { seed: 8872 })
      store = storeFactory.build(undefined, {
        localVue,
        state: { posts: { posts: [post] } }
      })
    })

    test('should match the snapshot', () => {
      expect.hasAssertions()

      const wrapper = mount(Post, {
        store,
        localVue,
        propsData: { post }
      })

      expect(wrapper).toMatchSnapshot()
    })
  })
})
