import { createLocalVue, mount } from '@vue/test-utils'

import AppHeader from '~/components/AppHeader.vue'
import { Store } from 'vuex'
import { StateType } from '~/logic/types'
import { storeFactory } from '~/test/fixtures/vuex'

const localVue = createLocalVue()

describe('AppHeader', () => {
  let store: Store<StateType>

  beforeEach(() => {
    store = storeFactory.build(undefined, { localVue })
  })

  describe('unit tests', () => {
    it('should be rendered successfully', () => {
      const wrapper = mount(AppHeader, {
        localVue,
        store,
        stubs: [
          'nuxt-link'
        ]
      })

      expect(Boolean(wrapper.vm)).toBe(true)
    })
  })

  describe('snapshot tests', () => {
    test('should match the snapshot', () => {
      const wrapper = mount(AppHeader, {
        localVue,
        store,
        stubs: [
          'nuxt-link'
        ]
      })

      expect(wrapper).toMatchSnapshot()
    })
  })
})
