import LocalService, { ILocalService } from '@/logic/services/local'
import { left, right } from 'fp-ts/lib/Either'

describe('LoocalService', () => {
  let service: ILocalService
  const key: string = 'testKey'

  beforeEach(() => {
    localStorage.clear()
    service = new LocalService()
  })

  afterAll(() => {
    localStorage.clear()
  })

  describe('key', () => {
    it('should be empty by default', () => {
      expect(service.key).toEqual('')
    })
  })

  describe('setLocalData', () => {
    beforeEach(() => {
      service.key = key
    })

    it('should set data to localStorage', () => {
      const value = 'testValue'

      service.setLocalData(value)
      expect(localStorage.setItem).toHaveBeenCalledWith(service.key, value)
      expect(localStorage.__STORE__[key]).toBe(value)
      expect(Object.keys(localStorage.__STORE__).length).toEqual(1)
    })
  })

  describe('getLocalData', () => {
    beforeEach(() => {
      service.key = key
    })
    it('should return error when where are no data in localStorage related the key', () => {
      expect(localStorage.__STORE__).toEqual({})
      const result = service.getLocalData()

      expect(localStorage.getItem).toHaveBeenCalledWith(service.key)
      expect(result).toEqual(left(Error(`There are no data in localStorage where key = ${service.key}`)))
    })

    it('should return value associated with key from localStorage', () => {
      const value = 'testValue'

      localStorage.setItem(service.key, value)
      expect(localStorage.__STORE__[key]).toBe(value)

      const result = service.getLocalData()

      expect(result).toEqual(right(value))
    })
  })

  describe('removeLocalData', () => {
    beforeEach(() => {
      service.key = key
    })
    it('should remove value associated with key from localStorage', () => {
      const value = 'testValue'

      localStorage.setItem(service.key, value)
      expect(localStorage.__STORE__[key]).toBe(value)

      service.removeLocalData()

      expect(localStorage.removeItem).toHaveBeenCalledWith(service.key)
      expect(localStorage.__STORE__).toEqual({})
    })
  })
})
