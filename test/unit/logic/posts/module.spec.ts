import { mock, when } from 'ts-mockito'
import { PostsModule } from '@/logic/posts/module'
import PostsService from '~/logic/posts/services/api'
import { RawPostType } from '~/logic/posts/models'

describe('PostsModule', () => {
  let postsModule: PostsModule

  beforeEach(() => {
    postsModule = new PostsModule()
  })

  describe('State', () => {
    it('should set initial state when module was initialised', () => {
      expect(postsModule.posts).toEqual([])
    })
  })

  describe('Getters', () => {
    describe('hasPosts', () => {
      it('shoudl return false when posts array is empty', () => {
        expect(postsModule.hasPosts).toEqual(false)
      })

      it('should return true when posts array contains 1 or more posts', () => {
        postsModule.setPosts([
          {
            id: 1,
            userId: 2,
            title: 'test',
            body: 'foo'
          }
        ])

        expect(postsModule.hasPosts).toEqual(true)
      })
    })
  })

  describe('Mutations', () => {
    describe('setPosts', () => {
      it('should set posts to the store when setPosts mutation is called', () => {
        const data = [
          {
            id: 1,
            userId: 2,
            title: 'test',
            body: 'foo'
          }
        ]

        postsModule.setPosts(data)

        expect(postsModule.posts).toEqual(data)
      })
    })
  })

  // describe('Actions', () => {
  //   describe('fetchPosts', () => {
  //     it('should get posts from PostsService', async () => {
  //       postsModule.service = mock(PostsService)
  //       const data: RawPostType[] = [
  //         {
  //           id: 1,
  //           userId: 2,
  //           title: 'test',
  //           body: 'foo'
  //         }
  //       ]
  //
  //       when(postsModule.service.fetchPosts()).thenResolve(data)
  //       postsModule.setPosts = jest.fn()
  //
  //       const result = await postsModule.fetchPosts()
  //
  //       expect(result).toEqual(data)
  //     })
  //   })
  // })
})
