import { createLocalVue } from '@vue/test-utils'
import axios from 'axios'
import { install as installDI } from '@/plugins/vue-typedi'
import AxiosMockAdapter from 'axios-mock-adapter'
import PostsService from '@/logic/posts/services/api'
import { RawPostType } from '~/logic/posts/models'

const localVue = createLocalVue()

describe('PostService', () => {
  let postService: PostsService
  let mockAxios: AxiosMockAdapter

  beforeEach(() => {
    jest.clearAllMocks()

    postService = new PostsService()
    mockAxios = new AxiosMockAdapter(axios)

    installDI(
      localVue,
      axios
    )
  })

  it('should return Posts when fetchPosts method is called', async () => {
    const data: RawPostType[] = [
      {
        id: 1,
        userId: 2,
        title: 'Hello',
        body: 'Hello World'
      }
    ]

    mockAxios.onGet('posts').reply(200, data)

    const result = await postService.fetchPosts()

    expect(result).toEqual(data)
  })
})
