import { left, right } from 'fp-ts/Either'

import { AuthModule } from '~/logic/auth/module'
import { RegistrationData, Credentials } from '~/logic/auth/types'

describe('AuthModule', () => {
  let authModule: AuthModule

  beforeEach(() => {
    authModule = new AuthModule()
  })

  describe('State', () => {
    it('should set initial state when module was initialised', () => {
      expect(authModule.token).toEqual('')
    })
  })

  describe('Mutations', () => {
    const token = 'token'
    it('should set token when setToken mutation is called', () => {
      authModule.setToken(token)

      expect(authModule.token).toEqual(token)
    })

    it('should clear token when clearToken mutation is called', () => {
      authModule.token = token

      authModule.clearToken()

      expect(authModule.token).toEqual('')
    })
  })

  describe('Actions', () => {
    describe('register', () => {
      const data: RegistrationData = {
        firstName: 'First',
        lastName: 'Last',
        middleName: 'Middle',
        email: 'email',
        password: 'password'
      }

      const token = 'token'
      const error = Error('Регистрация не удалась!')

      beforeEach(() => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        authModule.setToken = jest.fn((data) => {})
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        authModule.localService.setLocalData = jest.fn((data) => {})
      })

      it('should return token when registration is successful', async () => {
        authModule.apiService.register = jest.fn(async () => await right(token))

        const result = await authModule.register(data)

        expect(authModule.apiService.register).toHaveBeenCalledWith(data)
        expect(authModule.localService.setLocalData).toHaveBeenCalledWith(token)
        expect(authModule.setToken).toHaveBeenCalledWith(token)
        expect(result).toEqual(right(token))
      })

      it('should return Error when registration is unsuccessful', async () => {
        authModule.apiService.register = jest.fn(async () => await left(error))

        const result = await authModule.register(data)

        expect(authModule.apiService.register).toHaveBeenCalledWith(data)
        expect(authModule.localService.setLocalData).toHaveBeenCalledTimes(0)
        expect(authModule.setToken).toHaveBeenCalledTimes(0)
        expect(result).toEqual(left(error))
      })
    })

    describe('authenticate', () => {
      const data: Credentials = {
        email: 'email',
        password: 'password'
      }

      const token = 'token'
      const error = Error('Авторизация не удалась!')

      beforeEach(() => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        authModule.setToken = jest.fn((data) => {})
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        authModule.localService.setLocalData = jest.fn((data) => {})
      })

      it('should return token when authentication is successful', async () => {
        authModule.apiService.authenticate = jest.fn(async () => await right(token))

        const result = await authModule.authenticate(data)

        expect(authModule.apiService.authenticate).toHaveBeenCalledWith(data)
        expect(authModule.localService.setLocalData).toHaveBeenCalledWith(token)
        expect(authModule.setToken).toHaveBeenCalledWith(token)
        expect(result).toEqual(right(token))
      })

      it('should return Error when authentication is unsuccessful', async () => {
        authModule.apiService.authenticate = jest.fn(async () => await left(error))

        const result = await authModule.authenticate(data)

        expect(authModule.apiService.authenticate).toHaveBeenCalledWith(data)
        expect(authModule.localService.setLocalData).toHaveBeenCalledTimes(0)
        expect(authModule.setToken).toHaveBeenCalledTimes(0)
        expect(result).toEqual(left(error))
      })
    })

    describe('check', () => {
      const token = 'token'
      const error = Error('Не авторизован!')

      beforeEach(() => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        authModule.setToken = jest.fn((data) => {})
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        authModule.localService.setLocalData = jest.fn((data) => {})
        authModule.localService.getLocalData = jest.fn(() => right(token))
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        authModule.apiService.setTokenToDefaults = jest.fn((data) => {})
      })

      it('should return true when check is successful', async () => {
        authModule.apiService.check = jest.fn(async () => await right(token))

        const result = await authModule.check()

        expect(authModule.apiService.setTokenToDefaults).toHaveBeenCalledWith(token)
        expect(authModule.localService.setLocalData).toHaveBeenCalledWith(token)
        expect(authModule.setToken).toHaveBeenCalledWith(token)
        expect(result).toEqual(true)
      })

      it('should return false when there are no token in localStorage', async () => {
        authModule.localService.getLocalData = jest.fn(() => left(Error('There are no data in localStorage where key = auth')))

        const result = await authModule.check()

        expect(authModule.apiService.setTokenToDefaults).toHaveBeenCalledTimes(0)
        expect(authModule.setToken).toHaveBeenCalledTimes(0)
        expect(result).toEqual(false)
      })

      it('should return false when check is unsuccessful', async () => {
        authModule.apiService.check = jest.fn(async () => await left(error))

        const result = await authModule.check()

        expect(authModule.localService.setLocalData).toHaveBeenCalledTimes(0)
        expect(authModule.setToken).toHaveBeenCalledTimes(0)
        expect(result).toEqual(false)
      })
    })

    describe('logout', () => {
      const token = 'token'
      const error = Error('Не удалось выполнить выход!')

      beforeEach(() => {
        authModule.clearToken = jest.fn(() => {})
        authModule.localService.removeLocalData = jest.fn(() => {})
        authModule.localService.getLocalData = jest.fn(() => right(token))
        authModule.apiService.removeTokenFromDefaults = jest.fn(() => {})
      })

      it('should return true when logout is successful', async () => {
        authModule.apiService.logout = jest.fn(async () => await right(true))

        const result = await authModule.logout()

        expect(authModule.localService.removeLocalData).toHaveBeenCalledTimes(1)
        expect(authModule.apiService.removeTokenFromDefaults).toHaveBeenCalledTimes(1)
        expect(authModule.clearToken).toHaveBeenCalledTimes(1)
        expect(result).toEqual(true)
      })

      it('should return false when there are no token in localStorage', async () => {
        authModule.localService.getLocalData = jest.fn(() => left(Error('There are no data in localStorage where key = auth')))

        const result = await authModule.logout()

        expect(authModule.localService.removeLocalData).toHaveBeenCalledTimes(0)
        expect(authModule.apiService.removeTokenFromDefaults).toHaveBeenCalledTimes(0)
        expect(authModule.clearToken).toHaveBeenCalledTimes(0)
        expect(result).toEqual(false)
      })

      it('should return false when logout is unsuccessful', async () => {
        authModule.apiService.logout = jest.fn(async () => await left(error))

        const result = await authModule.logout()

        expect(authModule.localService.removeLocalData).toHaveBeenCalledTimes(0)
        expect(authModule.apiService.removeTokenFromDefaults).toHaveBeenCalledTimes(0)
        expect(authModule.clearToken).toHaveBeenCalledTimes(0)
        expect(result).toEqual(false)
      })
    })
  })
})
