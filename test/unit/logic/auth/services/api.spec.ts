import { createLocalVue } from '@vue/test-utils'
import axios from 'axios'
import { install as installDI } from '@/plugins/vue-typedi'
import AxiosMockAdapter from 'axios-mock-adapter'
import { AuthApiService } from '@/logic/auth/services'
import { left, right } from 'fp-ts/Either'
import { Credentials, RegistrationData } from '~/logic/auth/types'

const localVue = createLocalVue()

describe('AuthService', () => {
  let authService: AuthApiService
  let mockAxios: AxiosMockAdapter

  beforeEach(() => {
    jest.clearAllMocks()

    authService = new AuthApiService()
    mockAxios = new AxiosMockAdapter(axios)

    installDI(
      localVue,
      axios
    )
  })

  describe('setTokenToDefaults', () => {
    it('should set token to axios.defaults.headers.Authorization', () => {
      authService.setTokenToDefaults('token')

      expect(axios.defaults.headers.Authorization).toEqual('Bearer token')
    })
  })

  describe('removeTokenFromDefaults', () => {
    it('should remove token from axios.defaults.headers.Authorization', () => {
      authService.removeTokenFromDefaults()

      expect(axios.defaults.headers.Authorization).toEqual('')
    })
  })

  describe('register', () => {
    const data: RegistrationData = {
      firstName: 'First',
      lastName: 'Last',
      middleName: 'Middle',
      email: 'email',
      password: 'password'
    }

    it('should return true when registration is successful', async () => {
      mockAxios.onPost('auth/register').reply(201, { access_token: 'token' })
      authService.setTokenToDefaults = jest.fn()

      const result = await authService.register(data)

      expect(result).toEqual(right('token'))
    })

    it('should return false when registration is unsuccessful', async () => {
      mockAxios.onPost('auth/register').reply(422)

      const result = await authService.register(data)

      expect(result).toEqual(left(Error('Регистрация не удалась!')))
    })
  })

  describe('authenticate', () => {
    const data: Credentials = {
      email: 'email',
      password: 'password'
    }

    it('should return true when registration is successful', async () => {
      mockAxios.onPost('auth/login').reply(201, { access_token: 'token' })
      authService.setTokenToDefaults = jest.fn()

      const result = await authService.authenticate(data)

      expect(result).toEqual(right('token'))
    })

    it('should return false when registration is unsuccessful', async () => {
      mockAxios.onPost('auth/login').reply(422)

      const result = await authService.authenticate(data)

      expect(result).toEqual(left(Error('Авторизация не удалась!')))
    })
  })

  describe('check', () => {
    it('should return true when authentication check is successful', async () => {
      mockAxios.onGet('auth/check').reply(200, { access_token: 'token' })
      authService.setTokenToDefaults = jest.fn()

      const result = await authService.check()

      expect(result).toEqual(right('token'))
    })

    it('should return false when authentication check is unsuccessful', async () => {
      mockAxios.onGet('auth/check').reply(403)

      const result = await authService.check()

      expect(result).toEqual(left(Error('Не авторизован!')))
    })
  })

  describe('logout', () => {
    it('should return 200 when logout is successful', async () => {
      mockAxios.onGet('auth/logout').reply(200)

      const result = await authService.logout()

      expect(result).toEqual(right(true))
    })

    it('should return Error when logout is unsuccessful', async () => {
      mockAxios.onGet('auth/logout').reply(500)

      const result = await authService.logout()

      expect(result).toEqual(left(Error('Не удалось выполнить выход!')))
    })
  })
})
