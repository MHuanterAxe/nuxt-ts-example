import AuthLocalService from '@/logic/auth/services/local'

describe('AuthLocalService', () => {
  it('should have right key', () => {
    const authLocalService = new AuthLocalService()

    expect(authLocalService.key).toEqual('auth')
  })
})
